__author__ = 'Przemysław Kondaszewski'
__copyright__ = "Copyright (c) 2021 Przemysław Kondaszewski"
__email__ = "kondaszewskiprz@gmail.com"
__version__ = '1.0'

import argparse
import textwrap


def parse_args(args):
    parser = argparse.ArgumentParser(prog="Hanger console game", usage="guessing random words by revealing letters",
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent("""
                                                difficulty:            categories:                       letters:
                                                easy            - animals, plants, names, computer         3-5
                                                medium          - animals, plants, names, computer         6-9
                                                hard            - adjectives                               10+
                                                """))
    parser.add_argument("-d", "--Difficulty", type=str, help="Difficulty of the game")                             # str
    return parser.parse_args(args)
