__author__ = 'Przemysław Kondaszewski'
__copyright__ = "Copyright (c) 2021 Przemysław Kondaszewski"
__email__ = "kondaszewskiprz@gmail.com"
__version__ = '1.0'

import unittest
from unittest import TestCase
from main import game_logic


class MainTests(TestCase):

    def setUp(self) -> None:
        self.random_word = "woda"

        self.difficulty = "łatwy"

        self.blank_answer = "_ _ _ _"
        self.one_blank_answer = "w _ d a"

        self.output_word = "w o d a"

        self.correct_input_letter = "o"
        self.correct_input_word = "woda"
        self.incorrect_input_letter = "x"
        self.incorrect_input_word = "piach"

        self.char = "%"

        self.zero_lifes = 0
        self.one_life = 1
        self.three_lifes = 3
        self.eight_lifes = 8
        self.nine_lifes = 9

        self.empty_used_letters = []
        self.one_used_letters = ["x"]
        self.three_used_letters = ["x", "y", "j"]
        self.full_used_letters = ["t", "y", "j", "p", "r", "t", "v", "z"]

        self.return_1 = 1
        self.return_2 = 2
        self.return_4 = 4
        self.return_5 = 5
        self.return_6 = 6
        self.return_7 = 7

    # input   list, str     [random_word, category_info, answer, lifes, used_letters], letter
    # output  tuple         (int value for print output, answer (updated), lifes, used_letters)

    def test_correct_answer_with_word(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.blank_answer,
                                     self.zero_lifes, self.empty_used_letters], self.correct_input_word),
                         (self.return_1, self.blank_answer, self.zero_lifes, self.empty_used_letters))

    def test_wrong_answer_with_word(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.blank_answer,
                                     self.zero_lifes, self.empty_used_letters], self.incorrect_input_word),
                         (self.return_2, self.blank_answer, self.zero_lifes, self.empty_used_letters))

    def test_correct_answer_with_letter(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.one_blank_answer,
                                     self.zero_lifes, self.empty_used_letters], self.correct_input_letter),
                         (self.return_5, self.output_word, self.zero_lifes, self.empty_used_letters))

    def test_wrong_letter(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.one_blank_answer,
                                     self.zero_lifes, self.empty_used_letters], self.incorrect_input_letter),
                         (self.return_6, self.one_blank_answer, self.one_life, self.one_used_letters))

    def test_wrong_letter_9_lifes(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.one_blank_answer,
                                     self.eight_lifes, self.full_used_letters], self.incorrect_input_letter),
                         (self.return_4, self.one_blank_answer, self.nine_lifes, self.full_used_letters))

    def test_wrong_input_letter(self):
        self.assertEqual(game_logic([self.random_word, self.difficulty, self.one_blank_answer,
                                     self.three_lifes, self.three_used_letters], self.char),
                         (self.return_7, self.one_blank_answer, self.three_lifes, self.three_used_letters))


if __name__ == '__main__':
    unittest.main()
