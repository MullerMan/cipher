from sympy.crypto.crypto import encipher_vigenere, decipher_vigenere


def vigenere_encipher(string: str, key: list):
    enciphered_word = []
    for char in string:
        if char.isalpha():                                                     # enciphering only english alphabet
            enciphered_char = encipher_vigenere(char, str(key[0]))
            enciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            enciphered_word.append(char)
    return enciphered_word                                                     # return list with enciphered letters


def vigenere_decipher(string: str, key: list):
    deciphered_word = []
    for char in string:
        if char.isalpha():                                                     # deciphering only english alphabet
            enciphered_char = decipher_vigenere(char, str(key[0]))
            deciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            deciphered_word.append(char)
    return deciphered_word                                                     # return list with deciphered letters
