from sympy.crypto.crypto import encipher_shift, decipher_shift


def shift_encipher(string: str, key: list):
    enciphered_word = []
    for char in string:
        if char.isalpha():                                                     # enciphering only english alphabet
            enciphered_char = encipher_shift(char, int(key[0]))
            enciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            enciphered_word.append(char)
    return enciphered_word                                                     # return list with enciphered letters


def shift_decipher(string: str, key: list):
    deciphered_word = []
    for char in string:
        if char.isalpha():                                                     # deciphering only english alphabet
            enciphered_char = decipher_shift(char, int(key[0]))
            deciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            deciphered_word.append(char)
    return deciphered_word                                                     # return list with deciphered letters
