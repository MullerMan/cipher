from arper import parse_args
from file_operations import *
from error_handling import *

parser = parse_args(sys.argv[1:])


# cd C:\PyCharmProjects\Cipher\run_code

if __name__ == "__main__":
    if sys.argv[1] == "shift_encipher":
        shift_error_handling(str(sys.argv[2]), [sys.argv[3]])                  # (source: string, key: list)
        encipher_shift_file_operation(str(sys.argv[2]), [sys.argv[3]])
    elif sys.argv[1] == "shift_decipher":
        shift_error_handling(str(sys.argv[2]), [sys.argv[3]])
        decipher_shift_file_operation(str(sys.argv[2]), [sys.argv[3]])
    elif sys.argv[1] == "affine_encipher":
        affine_error_handling(str(sys.argv[2]), [sys.argv[3], sys.argv[4]])
        encipher_affine_file_operation(str(sys.argv[2]), [sys.argv[3], sys.argv[4]])
    elif sys.argv[1] == "affine_decipher":
        affine_error_handling(str(sys.argv[2]), [sys.argv[3], sys.argv[4]])
        decipher_affine_file_operation(str(sys.argv[2]), [sys.argv[3], sys.argv[4]])
    elif sys.argv[1] == "vigenere_encipher":
        vigenere_error_handling(str(sys.argv[2]), [sys.argv[3]])
        encipher_vigenere_file_operation(str(sys.argv[2]), [sys.argv[3]])
    elif sys.argv[1] == "vigenere_decipher":
        vigenere_error_handling(str(sys.argv[2]), [sys.argv[3]])
        decipher_vigenere_file_operation(str(sys.argv[2]), [sys.argv[3]])
    elif sys.argv[1] == "rsa_encipher":
        rsa_error_handling(str(sys.argv[2]), [sys.argv[3], sys.argv[4], sys.argv[5]])
        encipher_rsa_file_operation(str(sys.argv[2]), [sys.argv[3], sys.argv[4], sys.argv[5]])
    elif sys.argv[1] == "rsa_decipher":
        rsa_error_handling(str(sys.argv[2]), [sys.argv[3], sys.argv[4], sys.argv[5]])
        decipher_rsa_file_operation(str(sys.argv[2]), [sys.argv[3], sys.argv[4], sys.argv[5]])
    else:
        print("Option doesn't exist")
        sys.exit(0)
